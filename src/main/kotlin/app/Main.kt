package app

import app.core.Constants
import app.core.ResetEval
import app.core.console.Input
import kotlinx.serialization.ExperimentalSerializationApi
import sun.misc.Unsafe
import java.awt.GraphicsEnvironment
import java.lang.reflect.Field


/**
 * Full comments are not added yet to this, will comment some more soon
 * @author Kai
 */

class Main {

    /**
     * Main class
     * - Where the magic happens
     */
    companion object {
        @ExperimentalSerializationApi
        @JvmStatic
        fun main(args: Array<String>) {
            hideReflectionWarning()
            //Opens new command prompt window if running the .jar application without a console.
            if (System.console() == null && !GraphicsEnvironment.isHeadless() && Constants.IS_NOT_RUNNING_IN_IDE && Constants.isRunningJar()
                && !Constants.isHidden(args)) {
                Runtime.getRuntime().exec(Constants.DEFAULT_CMD_ARGUMENTS.plus(Constants.LAUNCH_JAR_ARGUMENTS))
            } else {
                Constants.PRINT_ASCII_TITLE
                checkJavaVersion() //Displays warning if running JDK 16 or higher (Reflection issues after JDK 8 {for Registry Edits})
                println()
                if(Constants.isHidden(args))
                    ResetEval.init(Constants.ALL_PRODUCTS) //Adding option to select single products in the future
                else {
                    Input().init()
                }
            }
        }

        /**
         * Displays warning if JDK is over 15.0.2 (1.15.0_02)
         * Application can't use reflection correctly from JDK16 - Working on this
         * Will still work for everything else, just not removing the registry keys
         */
        private fun checkJavaVersion() {
            val javaVersion = System.getProperty("java.version")
            val javaBuild = javaVersion.replace(".", "").split("_")[0]
            if(javaBuild.matches("-?\\d+(\\.\\d+)?".toRegex())) {
                val javaBuildAsInt = Integer.parseInt(javaBuild)
                if(javaBuildAsInt > Constants.JAVA_MAX_BUILD) {
                    writer(Constants.MAIN_CLASS, "COMPATABILITY - You are running on JRE/JDK [$javaVersion].")
                    writer(Constants.MAIN_CLASS, "COMPATABILITY - It is recommended to run this jar with JRE/JDK 15 (1.15.0_XX) or lower. Registry edits may not work." +
                            "\n" + "{IF YOU STILL EXPERIENCE ISSUES WITH ACTIVATION - PLEASE SEE MANUAL INSTRUCTIONS ON HOW TO REMOVE THE REGISTRY KEY")
                    println("\n")
                }
                Constants.JAVA_CURRENT_BUILD = javaBuildAsInt
            }
        }

        /**
         * In future versions of Java, reflection invoke methods may not be compatible
         * Tested JDK 8, 9, 11, 13, 14, 15 (Working)
         * JDK 16 (Not Working)
         */

        private fun hideReflectionWarning() {
            try {
                val unsafeField: Field = Unsafe::class.java.getDeclaredField("theUnsafe")
                unsafeField.isAccessible = true
                val unsafeObject = unsafeField.get(null) as Unsafe
                val illegalAccessLoggerClass = Class.forName("jdk.internal.module.IllegalAccessLogger")
                val loggedField : Field = illegalAccessLoggerClass.getDeclaredField("logger")
                unsafeObject.putObjectVolatile(illegalAccessLoggerClass, unsafeObject.staticFieldOffset(loggedField), null)
            } catch (e: Exception) { }
        }

        /**
         * Default writer for console output
         * Includes className
         */
        fun writer(className : String, line : String) {
            when { line.lowercase().contains("error") -> System.err.println("[$className]: $line") else -> println("[$className]: $line") }
        }

    }
}