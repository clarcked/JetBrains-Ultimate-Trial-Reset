package app.core

import app.Main

/**
 * @author Kai
 */
object Constants {

    const val ALL_PRODUCTS = "all-products" //All products only at the momo
    const val MAIN_CLASS = "Main" //Main Class
    fun isHidden(args : Array<String>) = args.contains("hidden")

    var JAVA_CURRENT_BUILD = -1
    const val JAVA_MAX_BUILD = 1502 //JDK 15.0.2 = 1502, JDK 1.8.0_281 = 180, JDK 9.0.4 = 904
    const val JAVA_11_BUILD = 1100 //JDK 11.0.0
    const val JAVA_16_BUILD = 1600 //JDK 16.0.0

    //Error messages & closes app if error returns
    const val PATH_DOES_NOT_EXIST = "Error - Path for JetBrains could not be found, you do not have any JetBrains products installed."
    const val PRODUCTS_DO_NOT_EXIST = "Error - Could not find any products that can be reset by this application. Please check the products.json file."

    //Eval key names within other.xml & Windows registry
    const val EVAL_REGISTRY_NAME = "evlsprt"

    //Checks if running in IntelliJ, if not will open a command prompt window
    val IS_NOT_RUNNING_IN_IDE = !System.getProperty("java.class.path").contains("idea_rt.jar") //CHANGE THIS TO FALSE (IF USING ANY OTHER IDE)

    //Root App Data Path (%AppData%/JetBrains)
    val ROOT_APP_DATA_DIRECTORY = "${System.getenv("APPDATA")}\\JetBrains"

    //Root Registry Directory (Computer\HKEY_CURRENT_USER\SOFTWARE\JavaSoft\Prefs\jetbrains)
    const val ROOT_REGISTRY_KEY_DIRECTORY = "SOFTWARE\\JavaSoft\\Prefs\\jetbrains"

    var DEFAULT_CMD_ARGUMENTS = arrayOf("cmd", "/c", "start", "cmd", "/k")
    var SAME_WINDOW_CMD_ARGUMENTS = arrayOf("cmd", "/c")

    val LAUNCH_JAR_ARGUMENTS = "java -jar \"${ Main::class.java.protectionDomain.codeSource.location.toString().substring(6)
        .replace("%20", " ") }\""

    //Returns true if running the .jar, false if running .class from IDE
    fun isRunningJar() : Boolean { return Main::class.java.getResource("Main.class")?.toString()!!.startsWith("jar:") }

    val PRINT_ASCII_TITLE = println("      _      _   ____            _             _____            _   ____                _   \n" +
            "     | | ___| |_| __ ) _ __ __ _(_)_ __  ___  | ____|_   ____ _| | |  _ \\ ___  ___  ___| |_ \n" +
            "  _  | |/ _ \\ __|  _ \\| '__/ _` | | '_ \\/ __| |  _| \\ \\ / / _` | | | |_) / _ \\/ __|/ _ \\ __|\n" +
            " | |_| |  __/ |_| |_) | | | (_| | | | | \\__ \\ | |___ \\ V / (_| | | |  _ <  __/\\__ \\  __/ |_ \n" +
            "  \\___/ \\___|\\__|____/|_|  \\__,_|_|_| |_|___/ |_____| \\_/ \\__,_|_| |_| \\_\\___||___/\\___|\\__|\n" +
            "                                                                                            ")

    val PRINT_COMMANDS_ASCII = println(
                "+-------------+--------------------------------------------------------------------------------------+----------------+\n" +
                "|   Command   |                                     Description                                      | ALLOWED INPUTS |\n" +
                "+-------------+--------------------------------------------------------------------------------------+----------------+\n" +
                "| RESET       | Resets the Evaluation Time for all installed JetBrains products on this machine.     | Yes, No        |\n" +
                "| NEW TASK    | Creates a monthly task which automatically resets the Evaluation Time every 30 days. | Yes, No        |\n" +
                "| DELETE TASK | Deletes all tasks created by this application.                                       | Yes, No        |\n" +
                "| EXIT        | Closes the application.                                                              | N/A            |\n" +
                "+-------------+--------------------------------------------------------------------------------------+----------------+"
    )
}