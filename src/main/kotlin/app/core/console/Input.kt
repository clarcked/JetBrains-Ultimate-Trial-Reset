package app.core.console

import app.core.Constants
import app.core.ResetEval
import app.core.windows.TaskScheduler
import kotlin.system.exitProcess

/**
 * @author Kai
 */
class Input {

    private val command = Command()

    fun init() { while(true) requestInput() }

    private fun requestInput() {
        Constants.PRINT_COMMANDS_ASCII
        print("Enter command: ")
        readLine()?.let { handleCommand(it) }
    }

    private fun handleCommand(userInput : String) {
        val cmd = getCommand(userInput.lowercase())
        if(cmd.contains("UNKNOWN COMMAND"))
            return
        else if(cmd.contentEquals(command.exit))
            command.execute(cmd, true)
        val question = cmd[1]
        val answerRequirement = cmd[2]

        println(question)

        val requestInput = readLine()
        if(answerRequirement.split(", ").contains(requestInput))
            command.execute(cmd, answerRequirement.contains("y") || answerRequirement.contains("yes"))
    }

    private fun getCommand(userInput : String): Array<String> {
        if(command.allowList.contains(userInput))
            when {
                command.resetEval[0].contains(userInput) -> return command.resetEval
                command.newTask[0].contains(userInput) -> return command.newTask
                command.deleteTask[0].contains(userInput) -> return command.deleteTask
                command.exit[0].contains(userInput) -> return command.exit
            } else
            println("\"{$userInput}\" is not a valid command.")
        return arrayOf("UNKNOWN COMMAND")
    }

    class Command {

        val resetEval = arrayOf(
            "reset", //[0] = Command
            "Would you like to reset the time of your JetBrains evaluation copy?", //[1] = Question
            "y, yes, n, no") //[2] = Answer

        val newTask = arrayOf(
            "new task", //[0] = Command
            "Would you like to schedule a task to automatically reset the JetBrains evaluation copy every month?", //[1] = Question
            "y, yes, n, no") //[2] = Answer

        val deleteTask = arrayOf(
            "delete task", //[0] = Command
            "Would you like to delete the existing task that is scheduled on your machine?", //[1] = Question
            "y, yes, n, no") //[2] = Answer

        val exit = arrayOf(
            "exit", //[0] = Command
            "N/A", //[1] = Question
            "N/A") //[2] = Answer

        val allowList = arrayOf(resetEval[0], newTask[0], deleteTask[0], exit[0])

        fun execute(command : Array<String>, canExecute : Boolean) =
            when {
                command.contentEquals(resetEval) && canExecute -> ResetEval.init(Constants.ALL_PRODUCTS)
                command.contentEquals(newTask) && canExecute -> TaskScheduler().scheduleTask()
                command.contentEquals(deleteTask) && canExecute -> TaskScheduler().deleteTask()
                command.contentEquals(exit) -> exitProcess(0)
                else -> null
            }
    }

}