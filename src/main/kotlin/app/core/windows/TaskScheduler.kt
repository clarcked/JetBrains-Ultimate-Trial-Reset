package app.core.windows

import app.core.Constants

/**
 * @author Kai
 */
class TaskScheduler {

    private val taskName = "Reset JetBrains Eval"
    private val jarLocation = "C:\\ProgramData\\JetBrainsEvalReset\\JetBrainsEvalReset.jar hidden"
    private val occurrence = "MONTHLY"
    private val time = "18:00"

    //SCHTASKS /CREATE /SC MONTHLY /TN "Reset JetBrains Eval" /TR "C:\ProgramData\JetBrainsEvalReset\JetBrainsEvalReset.jar" /ST 11:00
    private val create = "SCHTASKS /CREATE /SC $occurrence /TN \"$taskName\" /TR \"$jarLocation\" /ST $time"
    private val delete = "SCHTASKS /DELETE /TN \"$taskName\" /F"

    fun scheduleTask() {
        CloneJar().copy()
        Runtime.getRuntime().exec(Constants.SAME_WINDOW_CMD_ARGUMENTS.plus(create))
        println("Monthly task scheduled.")
    }

    fun deleteTask() {
        CloneJar().delete()
        Runtime.getRuntime().exec(Constants.SAME_WINDOW_CMD_ARGUMENTS.plus(delete))
        println("Monthly task deleted.")
    }

}