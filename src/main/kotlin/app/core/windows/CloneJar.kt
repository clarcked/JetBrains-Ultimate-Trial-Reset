package app.core.windows

import app.core.Constants
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption

/**
 * @author Kai
 */
class CloneJar {

    private val name = "JetBrainsEvalReset.jar"
    private val destinationPath = File("C:\\ProgramData\\JetBrainsEvalReset\\")

    fun copy() {
        if(!Constants.isRunningJar())
            return
        if(!destinationPath.exists())
            destinationPath.mkdir()
        val existingPath = File(CloneJar::class.java.protectionDomain.codeSource.location.toURI()).path
        val source = File(existingPath)
        val destination = File("${destinationPath.toPath()}\\$name")
        Files.copy(source.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING)
    }

    fun delete() {
        if(!Constants.isRunningJar())
            return
        if(destinationPath.exists())
            destinationPath.deleteRecursively()
    }

}