package app.core

import app.Main
import app.core.windows.Registry
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import sun.util.logging.PlatformLogger
import java.io.File
import kotlin.system.exitProcess

/**
 * @author Kai
 */
@ExperimentalSerializationApi
object ResetEval {

    private lateinit var productsDataArray : Array<Product>

    /**
     * Initializes the below methods
     */
    fun init(app : String) {
        Main.writer(Constants.MAIN_CLASS, "Initializing ResetEval...")
        Main.writer(this.javaClass.simpleName, "Loading list of JetBrains products from products.json.")
        loadProducts()

        Main.writer(this.javaClass.simpleName, "Scanning for JetBrains products.")
        val products = findLocalProducts(app)

        Main.writer(this.javaClass.simpleName, "Running product checks.")
        checks(products)

        Main.writer(this.javaClass.simpleName, "Found JetBrains products - deleting existing eval keys.")
        deleteEvalKey(products)
    }

    /**
     * Loads all known products from JetBrains (2021), within products.json
     */
    private fun loadProducts() {
        val productsDataFile = this::class.java.classLoader.getResource("products.json")?.readText()
        val json = Json { this.encodeDefaults = true }
        productsDataArray = json.decodeFromString(productsDataFile!!)
    }

    /**
     * Gets all installed JetBrains products from AppData
     */
    private fun findLocalProducts(app : String) : Array<String> {
        val defaultLocation = File(Constants.ROOT_APP_DATA_DIRECTORY)
        if(!defaultLocation.exists())
            return arrayOf(Constants.PATH_DOES_NOT_EXIST)
        else if(app == Constants.ALL_PRODUCTS) {
            var finalApps : Array<String> = emptyArray()
            for(product in defaultLocation.list()!!)
                repeat(productsDataArray.filter { product.lowercase().startsWith(it.app.lowercase()) }.size) { finalApps += (product) }
            return finalApps
        } else {
            for(product in defaultLocation.list()!!)
                if(product.startsWith(app))
                    return arrayOf(product)
        }
        return arrayOf(Constants.PRODUCTS_DO_NOT_EXIST)
    }

    /**
     * If JetBrains AppData is not on the local machine, will close application
     */
    private fun checks(product : Array<String>) {
        if (product.isNotEmpty()) {
            if (product[0] == Constants.PATH_DOES_NOT_EXIST) {
                Main.writer(this.javaClass.simpleName, Constants.PATH_DOES_NOT_EXIST)
                exitProcess(0)
            } else if (product[0] == Constants.PRODUCTS_DO_NOT_EXIST) {
                Main.writer(this.javaClass.simpleName, Constants.PRODUCTS_DO_NOT_EXIST)
                exitProcess(0)
            }
        } else { //isEmpty
            Main.writer(this.javaClass.simpleName, Constants.PRODUCTS_DO_NOT_EXIST)
            exitProcess(0)
        }
    }

    /**
     * Deletes the Eval Key from:
     * - AppData
     * - Registry
     * - XML
     */
    private fun deleteEvalKey(apps : Array<String>) {
       //Removes Eval Folder from Roaming App Data
        for(app in apps) {
            val evalKeyFolder = File(Constants.ROOT_APP_DATA_DIRECTORY + "\\" + app + "\\"+ "eval" )
            if(evalKeyFolder.exists())
                evalKeyFolder.deleteRecursively()
        }

        //Removes Eval Key from the Windows Registry
        //Default WinRegistry (bundled) does not come with methods required

       if(Constants.JAVA_CURRENT_BUILD < Constants.JAVA_16_BUILD) {
            val logger = PlatformLogger.getLogger("java.util.prefs")
            logger.setLevel(PlatformLogger.Level.SEVERE) //Hides prefs warning message
        }

        var keysToDelete : Array<String> = emptyArray()
        val rootRegistryKey = Registry.readStringSubKeys(Registry.HKEY_CURRENT_USER, Constants.ROOT_REGISTRY_KEY_DIRECTORY)

        for(root in rootRegistryKey!!) {
            val products = Registry.readStringSubKeys(Registry.HKEY_CURRENT_USER, Constants.ROOT_REGISTRY_KEY_DIRECTORY + "\\" + root)
            for(randomisedKey in products!!) {
                val finalPath = Constants.ROOT_REGISTRY_KEY_DIRECTORY + "\\" + root + "\\" + randomisedKey
                val evalKeys  = Registry.readStringSubKeys(Registry.HKEY_CURRENT_USER, finalPath)
                //FinalKey: SOFTWARE\\JavaSoft\\Prefs\\jetbrains\\idea\\a8a91490\\evlsprt
                evalKeys!!.filter { it.contains(Constants.EVAL_REGISTRY_NAME) }.forEach { keysToDelete += "${finalPath}\\${it}" }
            }
        }

        for(keyToDelete in keysToDelete) {
            if(keysToDelete.isEmpty())
                continue
            Registry.deleteKey(Registry.HKEY_CURRENT_USER, keyToDelete)
        }

        //Removes Eval Key from XML & rebuilds the XML file

        for(app in apps) {
            val optionsXml = File("${Constants.ROOT_APP_DATA_DIRECTORY}\\$app\\options\\other.xml")
            if (optionsXml.exists()) {
                var outputText = ""
                optionsXml.forEachLine {
                    if (!it.contains(Constants.EVAL_REGISTRY_NAME))
                        outputText += (if (it != "</application>") it + "\n" else it)
                }
                optionsXml.printWriter().use { out -> out.print(outputText) }
            }
        }
        Main.writer(this.javaClass.simpleName,"SUCCESS - RESET EVAL : ${apps.toList()}") //Fin
    }

}