package app.core

import kotlinx.serialization.Serializable

/**
 * @author Kai
 */
@Serializable
class Product(val build: String, val app : String)