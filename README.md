# JetBrains Ultimate Trial Reset (Evaluation Copy) - Kotlin

![image](https://user-images.githubusercontent.com/26250917/125868361-a4b26880-0cf2-4888-842f-b512b2ceabcd.png)

![image](https://user-images.githubusercontent.com/26250917/126057178-d85d030f-3048-4a20-bd9d-319639a3d047.png)


## About:

If you haven't had the chance to use the Eval trial for your JetBrains product, you can use this to reset the Eval Key.
Please do not abuse this and support the creators of the product by purchasing it from https://jetbrains.com (once the Eval copy is over).


## Download

* [JetBrainsEvalReset.jar](https://github.com/KaiBurton/JetBrainsEvalReset/raw/main/build/libs/JetBrainsEvalReset-1.0.3-SNAPSHOT.jar)

## Requirements:

* **Windows:** 7, 8, 8.1, 10 or 11.
* **Windows Server:** 2012, 2016, 2019 or 2022

**{REQUIRES JAVA 15.02 (1.15.0_02) OR LOWER TO MAKE MODIFICATIONS TO THE WINDOWS REGISTRY}**

* [Java 8 Download](https://www.java.com/download)

**IF RUNNING JDK 16 OR HIGHER:**

1. Navigate to reg-path: `Computer\HKEY_CURRENT_USER\SOFTWARE\JavaSoft\Prefs\jetbrains\{PRODUCT}\{RANDOM_CHARACTERS}`.
2. Delete the `evlsprt` keys.
3. Run `JetBrainsEvalReset.jar` before or after the key has been deleted.
4. Eval key should now be reset for all products.

(If you have multiple versions of Java installed, you can also force run JRE/JDK 8 by running the following command)
* In PowerShell: 
  * `Start-Process "C:\Program Files\Java\jre1.8.0_281\bin\javaw.exe" -ArgumentList "-jar .\JetBrainsEvalReset-1.0.3-SNAPSHOT.jar"`
* In Command Prompt: 
  * `call "C:\Program Files\Java\jre1.8.0_281\bin\javaw.exe" -jar .\JetBrainsEvalReset-1.0.3-SNAPSHOT.jar`
